def mergeSort(list_to_sort_by_merge):
    if len(list_to_sort_by_merge) <= 1:
        return # Just one or zero elements, nothing to sort

    # Split into right and left list and sort these recursively
    mid = len(list_to_sort_by_merge) // 2
    left = list_to_sort_by_merge[:mid]
    right = list_to_sort_by_merge[mid:]

    mergeSort(left)
    mergeSort(right)

    # Merge internally sorted left and right lists to create one sorted list
    l = 0 # Current position in the left list
    r = 0 # Current position in the right list
    i = 0 # Current position in the resulting list

    while l < len(left) and r < len(right): # Both partial lists still have elements
        if left[l] <= right[r]: # Select which element to append first
            list_to_sort_by_merge[i] = left[l]
            l += 1
        else:
            list_to_sort_by_merge[i] = right[r]
            r += 1
        i += 1

    # Once one partial list has run out just append the remaining elements of the other
    while l < len(left):
        list_to_sort_by_merge[i] = left[l]
        l += 1
        i += 1

    while r < len(right):
        list_to_sort_by_merge[i] = right[r]
        r += 1
        i += 1


# Test sorting by plotting a list before and after sorting
import matplotlib.pyplot as plt

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
x = range(len(my_list))

plt.scatter(x, my_list)
plt.title("Nicht-sortierte Liste")
plt.xlabel("Position in Liste")
plt.ylabel("Zahl")
plt.show()

mergeSort(my_list)

plt.title("Sortierte Liste")
plt.xlabel("Position in Liste")
plt.ylabel("Zahl")
plt.scatter(x, my_list)
plt.show()
